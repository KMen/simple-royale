var colors = require("colors");


var data = `
    ____             __        __  __                    __
   / __ \\____  _____/ /_____  / /_/ /   ____ _____  ____/ /
  / /_/ / __ \\/ ___/ //_/ _ \\/ __/ /   / __ \`/ __ \\/ __  / 
 / _, _/ /_/ / /__/ ,< /  __/ /_/ /___/ /_/ / / / / /_/ /  
/_/ |_|\\____/\\___/_/|_|\\___/\\__/_____/\\__,_/_/ /_/\\__,_/   
`.split("\n")

var colorz = ["grey", "white", "cyan", "blue", "green", "yellow", "red", "magenta"];
var sel = 0;
data.forEach((line) => {
	console.log("\t" + line[colorz[sel++]]);
});