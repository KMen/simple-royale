var librw = require("./lib/rw");
require("jsbytes");

if (!(process.argv[2] && process.argv[3])) {
	console.log(`USAGE:
	program <operation> <data>
		Operation can be:
			1 for writing, data must be number.
			2 for reading, data must be hex.
`)
	process.exit(1);
}
var operation = process.argv[2];
var data = process.argv[3];

switch (operation) {
	case "1":
		write(data);
		break;
	case "2":
		read(data);
		break;
	default:
		console.log(`USAGE:
	program <operation> <data>
		Operation can be:
			1 for writing, data must be number.
			2 for reading, data must be hex.
`);
		break;
}

function read (d) {
	try {
		var b = Buffer(d, "hex");
	} catch (e) {
		console.log("Error, make sure d is a hex string.");
		process.exit(1);
	}

	var r = new librw.Reader(b);
	try {
		r.readRrsint("value");
	} catch (e) { 
		console.log("Error, make sure d is a hex string AND a valid VInt.");
		process.exit(1);
	}
	console.log(r.JSON.value);
}

function write (d) {
	try {
		var w = new librw.Writer();
		w.writeVInt(Number(d));
	} catch (e) {
		console.log("Error, make sure d is a number.");
		process.exit(1);
	}

	console.log(w.Buf.toString("hex"));
}
