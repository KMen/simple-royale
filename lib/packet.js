module.exports.split = (raw) => {
	var keepGoing = true;
	var _outs = [];
	var offset = 0;

	while(keepGoing) {
		var _out = {};
		_out.ID = int.from_bytes(raw.slice(offset, offset + 2), "big");
		offset += 2;
		_out.sentlength = int.from_bytes(raw.slice(offset, offset + 3), "big");
		offset += 3;
		_out.sentversion = int.from_bytes(raw.slice(offset, offset + 2), "big");
		offset += 2;

		_out.payload = raw.slice(offset, _out.sentlength + offset);
		offset += _out.sentlength;

		_outs.push(_out);
		if (raw.length - offset < 1) {
			keepGoing = false;
		}
	}

	return _outs;
}

module.exports.join = (id, pay) => {
	return Buffer.concat([
		id.to_bytes(2, "big"),
		pay.length.to_bytes(3, "big"),
		int(3).to_bytes(2, 'big'),
		pay	
	]);
}