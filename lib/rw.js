var ByteBuffer = require('bytebuffer');
var zlib = require('zlib');

class reader {
    constructor(Buf) {
        if (!Buffer.isBuffer(Buf)) {
            throw new TypeError("Not a buffer")
        };
        this.orig = Buf;
        this.rem = Buf;
        this.JSON = {};
        this.asArray = [];
    }
    
    getBytes(n) {
        if (this.rem.length < n) {
            throw new RangeError(`Could not get ${n} bytes from Buffer. \n ${this.rem.toString('hex')} \n\n ${JSON.stringify(this.JSON)}`);
            return false;
        }

        var got = this.rem.slice(0, n);
        this.rem = this.rem.slice(n);
        return got;
    }

    setJSON(pname, data) {
        if (pname) {
            this.JSON[pname] = data
        }
    }

    readInt32(pname = false) {
        var i = int.from_bytes(this.getBytes(4), 'big');
        this.asArray.push(i);
        this.setJSON(pname, i);
        return i;
    }

    readLong(pname = false) {
        var o = int.from_bytes(this.getBytes(8), 'big');
        this.setJSON(pname, o);
        this.asArray.push(o);
        return o;
    }

    readString(pname = false) {
        var l = this.readInt32();
        if (l == 0xFFFFFFFF) {
            return ""
        }
        var s = this.getBytes(l);
        this.setJSON(pname, s.toString('utf8'));
        this.asArray.push(s.toString('utf8'));
        return s.toString('utf8');
    }

    readRrsint(pname = false) 
    {
        var num1 = this.readByte();
        var num2 = num1 & 128;
        var num3 = num1 & 63;
        if ((num1 & 64) != 0)
        {
            if (num2 != 0)
            {
                var num4 = this.readByte();
                var num5 = num4 << 6 & 8128 | num3;
                if ((num4 & 128) != 0)
                {
                    var num6 = this.readByte();
                    var num7 = num5 | num6 << 13 & 1040384;
                    if ((num6 & 128) != 0)
                    {
                        var num8 = this.readByte();
                        var num9 = num7 | num8 << 20 & 133169152;
                        if ((num8 & 128) != 0)
                        {
                            var num10 = this.readByte();
                            num3 = ((num9 | num10 << 27) | 2147483648);
                        }
                        else
                            num3 = (num9 | 4160749568);
                    }
                    else
                        num3 = (num7 | 4293918720);
                }
                else
                    num3 = (num5 | 4294959104);
            }
        }
        else if (num2 != 0)
        {
            var num4 = this.readByte();
            num3 |= num4 << 6 & 8128;
            if ((num4 & 128) != 0)
            {
                var num5 = this.readByte();
                num3 |= num5 << 13 & 1040384;
                if ((num5 & 128) != 0)
                {
                    var num6 = this.readByte();
                    num3 |= num6 << 20 & 133169152;
                    if ((num6 & 128) != 0)
                    {
                        var num7 = this.readByte();
                        num3 |= num7 << 27;
                    }
                }
            }
        }
        return num3;

        this.getBytes(offset);
        this.setJSON(pname, value);
        this.JSON[pname] = value;

        return value;
    };

    readBool(pname = false) {
        var b = this.getBytes(1);
        if (b != 0x00) {
            this.setJSON(pname, true);
            return true
        } else {
            this.setJSON(pname, true);
            return false
        }
    }

    readVInt(pname = false) {
        return this.readRrsint(pname);
    }

    readByte (pname = false) {
        var b = this.getBytes(1);
        var asi = int.from_bytes(b, "big");
        this.setJSON(pname, asi);
        return asi;
    }
}

class writer {
    constructor(ExistingBuffer = false) {
        if (ExistingBuffer) {
            this.Buf = Buffer(ExistingBuffer)
        } else {
            this.Buf = Buffer.alloc(0)
        }
        this.JSON = {};
        this.asArray = [];
        this.ID = 0;
    }

    appendBuffer(NewBuffer, pname = null, Data = null) {
        if (pname && (Data !== null)) {
            this.JSON[pname] = Data;
            this.asArray.push(Data);
        }
        try {
            this.Buf = Buffer.concat([this.Buf, NewBuffer])
        } catch (error) {
            console.error(error.stack);
            return false;
        }
        return this;
    }

    setID (id) {
        this.ID = Number(id);
        return this;
    }

    writeInt32(n, pname = false) {
        if (!n) {n=0}
        return this.appendBuffer(int(n).to_bytes(4, 'big'), pname, n);
    }

    writeLong(n, pname = false) {
        if (!n) {n=0}
        return this.appendBuffer(int(n).to_bytes(8, 'big'), pname, n);
    }

    writeString(s, pname = false) {
        if (!s) {s=""};

        var BL = int(
            (s.length > 1) ? s.length : 0xFFFFFFFF
        ).to_bytes(4, 'big');
        return this.appendBuffer(Buffer.concat([BL, Buffer(s, 'utf8')]), pname, s);
    }

    writeBool(b, pname = false) {
        if (b) {
            return this.appendBuffer(Buffer([0x01]), pname, true);
        } else {
            return this.appendBuffer(Buffer.alloc(1), pname, false);
        }
    }

    writeHex(h, pname = false) {
        if (!h) {h=""}
        return this.appendBuffer(
            Buffer(h.split(" ").join("").split('\r').join('').split('\t').join('').split('\n').join(''), 'hex'), pname, h
        )
    }

    writeByte(b, pname = false) {
        if (!b) {b=0}
        return this.appendBuffer(Buffer([b]))
    }

    writeRrsint(Value)
    {
        if (Value > 63) {
            this.writeByte((Value & 0x3F | 0x80));
            
            if (Value > 8191) {
                this.writeByte((Value >> 6 | 0x80));
    
                if (Value > 1048575) {
                    this.writeByte((Value >> 13 | 0x80));
    
                    if (Value > 134217727) {
                        this.writeByte((Value >> 20 | 0x80));
                        Value >>= 27 & 0x7F;
                    } else {
                        Value >>= 20 & 0x7F;
                    }
                } else {
                    Value >>= 13 & 0x7F;
                }
            } else {
                Value >>= 6 & 0x7F;
            }
        }
        return this.writeByte(Value);
    }

    writeSCID(high, low) {
        if(!high) {high = 0}
            if(!low) {low = 0}
        this.writeRrsint(high);
        this.writeRrsint(low);
        return this;
    }

    writeVInt (n, pname = false) {
        if (!n) {n=0}
        return this.writeRrsint(n, pname);
    }

    writeZString (s, pname = false) {
        var compressed = zlib.deflateSync(s);

        this.writeInt32(compressed.length + 4);
        this.appendBuffer(s.length.to_bytes(4, "little"));
        this.appendBuffer(compressed);
        return this;
    }

    compressAll () {
        var len = this.Buf.length;

        var compressed  = zlib.deflateSync(this.Buf);
        this.Buf = Buffer.alloc(0);
        this.writeBool(true);
        this.appendBuffer(int(len).to_bytes(4, "little"));
        this.appendBuffer(compressed);
        return this;
    }

    writeRrslong (n, pname = false) {
        var long = n.to_bytes(8, "big");
        var L_HI = int.from_bytes(long.slice(0,4), "big");
        var L_LO = int.from_bytes(long.slice(4), "big");

        this.writeVInt (L_HI);
        return this.writeVInt (L_LO);
    }
}
module.exports.Reader = reader;
module.exports.Writer = writer;
