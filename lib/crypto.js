const RC4 = require("simple-rc4");
const baseKey = "fhsd6f86f67rt8fw78fw789we78r9789wer6re";
const nonce = "nonce";

var Crypto = () => {
	var g = new RC4(baseKey + nonce, "utf8");
	g.update(baseKey + nonce);
	return g;
}

global.modcrypto = {//Rc4 crypto object
    "init": function () {
        var g = new RC4(Buffer(baseKey + "nonce", "utf8"));
        g.update(Buffer(baseKey + "nonce", "utf8"));
        return g;
    }  
}

module.exports = Crypto;