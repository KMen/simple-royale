require("colors");
const path = require("path");

class optionGetter {
	constructor (opts, defaults) {
		this.opts = opts;
		this.defaults = defaults;
	}

	get (name) {
		return this.opts[name] ? this.opts[name] : (this.defaults[name] ? this.defaults[name] : null);
	}
}

global.optionGetter = optionGetter;
global.msgs = require("../enums/msgs");

function get_handler (ID) {
	if (!msgs.client[ID]) {
		return {error: {message: "ID Unknown", code: 1}};
	}

	try {
		var handler = require("./messages/client/" + msgs.client[ID].toString());
	} catch (e) {
		if (e.code == "MODULE_NOT_FOUND") {
			return {error: {message: "Packet Not Handled", code: 2}};
		} else {
			return {error: {error: e, message: "Unknown Error", code: 3}};
		}
	}

	return {handle: handler.main, code: 0};
}


function handle (in_packet) { //in_packet.reader = librw.Reader, in_packet.writer = librw.Writer, in_packet.client = _clients[cKey], in_packet.cKey = cKey;
	try {
		var handler = get_handler(in_packet.ID);

		if (handler.error) {
			switch (handler.error.code) {
				case 1:
					console.error(`ID ${in_packet.ID.toString().red} is unknown.`);
					return [];
				case 2: 
					console.error(`"${msgs.client[in_packet.ID].red}" Message (${in_packet.ID.toString().red}) was not Handled!`);
					return [];
				case 3:
					console.error(`An unknown error occurred while handling ${in_packet.ID.toString().red}`);
					throw handler.error.error;
					return [];
				default:
					return [];
			}
		} else {
			//everything should be in here
			console.log(`Handling ${msgs.client[in_packet.ID].green} (${in_packet.ID.toString().green})...`)
			return handler.handle(in_packet);
		}



	} catch (err) {
		console.error(err.stack.red);
		_clients[in_packet.cKey].connection.destroy();
		_clients[in_packet.cKey] = {};
		delete _clients[in_packet.cKey]
	}
}; //must return [librw.Writer] array.

module.exports.handle = handle;