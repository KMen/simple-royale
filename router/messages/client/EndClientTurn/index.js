var CommandHandler = require("../../../commands/index.js");

module.exports.main = (in_packet) => {

	var r = new librw.Reader(in_packet.payload);

	var Tick = r.readRrsint("tick");
	var Checksum = r.readRrsint("checksum");
	var Count = r.readRrsint("count");
	var _outs = [];
	var rets = [];

	if (Count > 0 && Count <= 512) {
		for (var i = 0; i < Count; i++) {
			try {
				var CommandID = r.readRrsint();
			} catch(e) {
				throw Error("invalid command");
			}
			var thisout = CommandHandler.handle({reader:r, client:in_packet.client, cKey:in_packet.cKey, ID:CommandID});
			if (thisout) {
				_outs.push(thisout);
			}
		}

		_outs.forEach( (out) => {
			if (out.messages) {
				out.messages.forEach ( (msg) => {
						rets.push(msg)
				} );
			}
			else {
				var W = new librw.Writer();
				W.setID(24111)
					.writeVInt(out.ID)
					.appendBuffer(out.payload);
				rets.push(W);
			}
		});
		return rets;
	} 
	
}