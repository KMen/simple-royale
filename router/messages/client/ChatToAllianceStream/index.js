module.exports.main = (in_packet) => {
	var r = new librw.Reader(in_packet.payload);
	var message = r.readString();

	switch (message) {
		case "/www":
			var o = require("../../server/LoginFailed").LoginFailed({
				ReasonCode: 8,
				updateURL: "http://www.rocketland.tk",
				reason: "Click UPDATE to visit our website - www.rocketland.tk",
			});
			return [ o ];
		case "/lol":
			var o = require("../../server/BattleNPC/SectorStateNPC")(in_packet);
			return [o];
		default:
			return [];
	}
}