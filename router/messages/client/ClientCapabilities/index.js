module.exports.main = (in_packet) => {
	var r = in_packet.reader;
	r.readVInt("ping");
	r.readString("interface");

	console.log(`ClientCapabilities had a ping time of ${r.JSON.ping}, over the connection ${r.JSON.interface}`.cyan);

	return [];
}