function SectorStateNPC(in_packet)
{
    var o = new librw.Writer();
    var Count = 6;

    o.setID(21903)

        .writeBool(false)
        .writeVInt(0)

        .writeVInt(21)
        .writeVInt(11)
        .writeVInt(0)
        .writeHex("d601bd9c86999497") //Checksum?

        .writeVInt(6)
        .writeVInt(1)
        .writeHex("7f7f7f7f0000") // Enemy ID
        .writeString("")
        
        //.writeVInt(21)
        //.writeVInt(9999)
        //01 32 //1 50

        .writeVInt(1)
        .writeVInt(50)

        //12-block
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)


        .writeVInt(0)//WTF an extra 0x00
        //.writeVInt(8)
        .writeVInt(7)

        //7-block
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)


        .writeVInt(0)//WTF an extra 0x00
        //.writeVInt(2)
        .writeVInt(11)


        //10-block
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)


        .writeVInt(0)//WTF an extra 0x00
        //.writeVInt(1)
        .writeVInt(4)



        .writeVInt(2)
        .writeRrslong(in_packet.client.data.id)
        .writeRrslong(in_packet.client.data.id)
        .writeRrslong(in_packet.client.data.id)
        
        /*.writeHex(`

        02    
        23 9a 99 9e 11   
        23 9a 99 9e 11   
        23 9a 99 9e 11

        `)*/
        
        .writeString(in_packet.client.data.HomeData.name) //name
        .writeHex("020000000000000000230000000000080605018803051d8888d544050d00050e0405030105020500000002050b23051b01071a00091a010a1a03041a0d0a1c01021a0e021a0c020000010000000000000005000000010002")
        
        .writeVInt(2)//player amount
        .writeHex("08 01 7f7f 00")

        //.writeHex("23 9a 99 9e 11 ")
        .writeRrslong(in_packet.client.data.id)


        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)

        .writeBool(true) //isTrainer

        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(0)


        // backup file line 109

        .writeVInt(142)
        .writeVInt(62077)
        //8e02f27d


        .writeVInt(0)
        .writeVInt(0)

        .writeVInt(Count)
        .writeVInt(Count)


        .writeVInt(35)
        .writeVInt(1)

        .writeVInt(35)
        .writeVInt(1)

        .writeVInt(35)
        .writeVInt(1)

        .writeVInt(35)
        .writeVInt(1)

        .writeVInt(35)
        .writeVInt(0)

        .writeVInt(35)
        .writeVInt(0)


        .writeVInt(1)
        .writeVInt(0)
        .writeVInt(1)
        .writeVInt(0)
        .writeVInt(0)
        .writeVInt(1);

        for (var Index = 0; Index < Count; Index++)
        {
            o.writeVInt(5)
                .writeVInt(Index);
        }
        
         
        var stamps = ["00007f00c07c0002", "00007f0080040001", "00007f00c07c0001", "00007f0080040002"];

        var coordinates = [
            ["a4e201", "9c8e03"],
            ["ac36", "a465"],
            ["ac36", "9e8e03"],
            ["a4e201", "a465"]
        ];
	var lvls = [12, 12, 12, 12];

	for (var i = 0; i < coordinates.length; i++) {
            o.writeVInt(lvls[i]); // level
            o.writeVInt(13); // Prefix
            o.writeHex(coordinates[i][0]); // x
            o.writeHex(coordinates[i][1]); // y
            o.writeHex(stamps[i]);
            o.writeHex("000000000000");
        }


        //o.writeHex("00 0d a4e201 9c8e03  00007f00c0 7c0002000000000000       01 0d ac36 a465 00007f0080  04 0001000000000000        00 0d ac36 9c8e03 00007f00c0 7c0001000000000000          01 0d a4e201 a465 00007f0080  04 0002000000000000")
	
	
        o.writeHex("000da88c01b82e00007f008004000000000000000000050403047d7c0405020106007f7f000000")
       
        .writeVInt(10) // Elixir Start
        .writeHex("00000000007f7f7f7f7f7f7f7f")




        .writeVInt(20) //crown tower level?
        .writeVInt(19) //prefixed?? // crown tower level - 1??
        .writeHex("0da88c0188c50300007f00c07c0000000000000000000504077d7d050403000205007f7f000000")
        .writeVInt(10) // Elixir Start
        .writeHex("00000000007f7f7f7f7f7f7f7f")







        .writeHex("000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000")//b81500
	
	.writeVInt(1512)//btm right princess twr. hp
	.writeByte(0)

	.writeVInt(1)//top left princess twr. hp
	.writeByte(0)

	.writeVInt(1512)//btm left princess twr. hp
	.writeByte(0)

	.writeVInt(1)//top right princess twr. hp
	.writeHex("00     01     00a0250000000000000000a401a40100000000000000a401a40100000000000000a401a40100000000000000a401a40100000000000000a401a40100000000000000a401a401ff01010102010301140104003c00890101100000fe03010102010e018a010189010004000f000d000000050602020402010300000000000000060100000900000c000000a8dd97d40d002a002b")
        .compressAll();


    return o;
}

module.exports = SectorStateNPC;

