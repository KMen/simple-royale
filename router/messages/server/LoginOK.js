function LoginOK(opts) {
	var o = new librw.Writer();
	var ogetter = new optionGetter (opts, {
		id: 0,
		passToken: "hello",
		gameCenterID: "",
		facebookID: "",
		serverMajorVersion: 0,
		serverBuild: 0,
		contentVersion: 0,
		environment: "",
		sessionCount: 0,
		playTimeSeconds: 0,
		accountAgeDays: 0,
		facebookAppID: "",
		serverTime: String(Math.round(new Date().getTime() / 1000)),
		accountCreationTime: String(Math.round(new Date().getTime() / 1000)),

		googleID: "",


		region: "",
		contentURL: "",
		eventAssetsURL: "",

	});

	o.setID(20104)
		.writeLong(ogetter.get("id"))
		.writeLong(ogetter.get("id"))
		.writeString(ogetter.get("passToken"))
		.writeString(ogetter.get("gameCenterID"))
		.writeString(ogetter.get("facebookID"))
		.writeVInt(ogetter.get("serverMajorVersion"))
		.writeVInt(ogetter.get("serverBuild"))
		.writeVInt(ogetter.get("serverBuild"))
		.writeVInt(ogetter.get("contentVersion"))
		.writeString(ogetter.get("environment"))
		.writeVInt(ogetter.get("sessionCount"))
		.writeVInt(ogetter.get("playTimeSeconds"))
		.writeVInt(ogetter.get("accountAgeDays"))
		.writeString(ogetter.get("facebookAppID"))
		.writeString(ogetter.get("serverTime"))
		.writeString(ogetter.get("accountCreationTime"))
		.writeVInt(0)
		.writeString(ogetter.get("googleID"))
		.writeString("")
		.writeString("")
		.writeString(ogetter.get("region"))
		.writeString(ogetter.get("contentURL"))
		.writeString(ogetter.get("eventAssetsURL"))
		.writeByte(0);
	return o;
}

module.exports.ok = (in_packet, opts) => {
	var o = LoginOK(opts);
	return o;
}