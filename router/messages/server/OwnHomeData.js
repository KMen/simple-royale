var cards = require("../../../enums/cards.json");
var shortcards = require("../../../enums/shortcards.js");
var allcards = require("../../../enums/cardarray.js");

global.COMMON_CARD_MAX_COUNT = 958;


class TOURNEY_STD {
	constructor (name, rarity) {
		this.Name = name;
		this.Count = 0;
		switch (rarity) {
			case "Legendary":
				this.Level = 1;
				break;
			case "Epic":
				this.Level = 4;
				break;
			case "Rare":
				this.Level = 7;
				break;
			case "Common":
				this.Level = 9;
				break;
			
		}
	}
}

class MAXED_CARD {
	constructor (name, rarity) {
		this.Name = name;
		this.Count = 0;
		switch (rarity) {
			case "Legendary":
				this.Level = 5;
				break;
			case "Epic":
				this.Level = 8;
				break;
			case "Rare":
				this.Level = 11;
				break;
			case "Common":
				this.Level = 13;
				break;
			
		}
	}
}

function OwnHomeData (opts) {
	var o = new librw.Writer();
	var ogetter = new optionGetter (opts, {
		userID: 0,
		Name: "RocketLand",
		Trophies: 6400,
		XP: 79999,
		Level: 12,
		Gold: 1000000,
		Gems: 34822,
		donationCooldown: 6,
		donationCapacity: 0,
		TimeStamp: Date.now(),
		Clan: {
			Name: "www.rocketland.tk"
		},
		TrainingProgress: 8,
		Deck: [
			new TOURNEY_STD("IceSpirits", "Common"),
			new TOURNEY_STD("Skeletons", "Common"),
			new TOURNEY_STD("Bats", "Common"),
			new TOURNEY_STD("Goblins", "Common"),
			new TOURNEY_STD("FireSpirits", "Common"),
			new TOURNEY_STD("Zap", "Common"),
			new TOURNEY_STD("SpearGoblins", "Common"),
			new TOURNEY_STD("Log", "Legendary")
			
		],

		CardsAfterDeck: [
			//new MAXED_CARD("ZapMachine", "Legendary")
			new TOURNEY_STD("Mirror", "Epic"),
			new TOURNEY_STD("IceGolemite", "Rare"),
			new TOURNEY_STD("Rage", "Epic"),
			new TOURNEY_STD("Knight", "Common"),
			new TOURNEY_STD("Minions", "Common"),
			new TOURNEY_STD("Archer", "Common"),
			new TOURNEY_STD("Clone", "Epic"),
			new TOURNEY_STD("Tornado", "Epic"),
			new TOURNEY_STD("Heal", "Rare"),
			new MAXED_CARD("Rocket", "Rare"),
			new TOURNEY_STD("MegaMinion", "Rare"),
			new TOURNEY_STD("BlowdartGoblin", "Rare"),
			new TOURNEY_STD("GoblinGang", "Common"),
			new TOURNEY_STD("GoblinBarrel", "Epic"),
			new TOURNEY_STD("Arrows", "Common"),
			new TOURNEY_STD("Tombstone", "Rare"),
			new TOURNEY_STD("SkeletonArmy", "Epic"),
			new TOURNEY_STD("Bomber", "Common")
		],

		Shop: [
			"Knight",
			"Archer",
			"Goblins",
			"MegaKnight",
			"Zap",
			"Assassin"
		]
	});

	o.setID(24101)

		.writeLong(ogetter.get("userID"))
		.writeVInt(0)
		.writeVInt(0)
		.writeVInt(ogetter.get("donationCooldown"))
		.writeVInt(ogetter.get("donationCapacity"))
		.writeVInt(ogetter.get("TimeStamp"))
		.writeVInt(0)
		.writeVInt(0, "DeckCount_decks_go_here")
		.writeByte(255);

	function addShortCard (ewriter, cardopts) {
		var cogetter = new optionGetter(cardopts, {
			Name: "Knight",
			Level: 1,
			BoughtTimes: 0,
			Count: 0,
			Status: 0
		});
		
		
		try {var cardI = shortcards[cogetter.get("Name")];ewriter.writeVInt(cardI[0]);}
		catch (err) {throw new Error(`Could not get card ${cogetter.get("Name")}`)}
		

		//if (cardI[1] > 26) {
		//	ewriter.writeBool(true);
		//}

		ewriter.writeByte(cogetter.get("Level") - 1)
			.writeVInt(cogetter.get("BoughtTimes"))
			.writeVInt(cogetter.get("Count"))
			.writeHex("0000")
			.writeByte(cogetter.get("Status"));
		return ewriter;
	}
	
	for (var i = 0; i < 8; i++) {
		addShortCard(o, {
			Name: allcards[i],
			Level: 1,
			Count: 10000
		});
	} //deck

	o.writeVInt(allcards.length - 8);

	for (var i = 8; i < allcards.length; i++) {
		addShortCard(o, {
			Name: allcards[i],
			Level: 1,
			Count: 10000
		});
	}
	




	/*ogetter.get("Deck").forEach((card) => {
		addShortCard(o, card);
	});*/
	

	/*o.writeVInt(ogetter.get("CardsAfterDeck").length, "CardsAfterDeckCount");

	ogetter.get("CardsAfterDeck").forEach((card) => {
		addShortCard(o, card);
	});*/

	o.writeHex("03ff2b007f0000000004007f0000000012007f000000000e007f000000009301007f0000000009007f0000000017007f000000008901007f00000000099811")

		.writeVInt(ogetter.get("TimeStamp"))
		.writeVInt(1)
		.writeVInt(0)

		.writeVInt(0, "ChallengeCountPlusEventCount")
		.writeHex("00000000040080b692850b0000000a8309008409008509009311019511019611019711019811019911019a1101079311039511029611039711029811029911029a110102")
		//^Unknown
		.writeString('{"ID":"CARD_RELEASE","Params":{}}')
		.writeVInt(0) // 4 = Enable Clan Chest Below
		//.writeString("{\"ID\":\"CLAN_CHEST\",\"Params\":{\"StartTime\":\"20170317T070000.000Z\",\"ActiveDuration\":\"P3dT0h\",\"InactiveDuration\":\"P4dT0h\",\"ChestType\":[\"ClanCrowns\"]}}")
		.writeHex("04008094238094238797c4960b00007f0113070102007f000000000000000000000000000098f7d20198f7d20185fccc960b98f7d20198f7d20185fccc960b0000007f000000000000000000")
		
		.writeByte(ogetter.get("Level"))
		
		.writeHex("009ebb8583060600")
		.writeVInt(900 * 20) //secs until shop refresh * 20 (aka ticks)
		.writeVInt(900 * 20) //secs until shop refresh * 20 (aka ticks)
		
		.writeVInt(ogetter.get("TimeStamp"));
	
	o.writeByte(ogetter.get("Shop").length)
	var shopindex = 0;
	ogetter.get("Shop").forEach((cardname) => {
		o.writeHex("01820102")
			.writeVInt(0) // Number Sold
			.writeHex("0000000000")
			.writeByte(shortcards[cardname][1]) //hi
			.writeByte(shortcards[cardname][2]) //lo
			.writeByte(shopindex++)
	});

		o.writeHex("0000007f00007f00007f00000000000000000000000a00000000f80101000001000000020000010000000e0000010000008a0100000100000089010000010000000400000100000000000000000000000100000001000000000000000000000001")
			.writeHex("33bdadd608 33bdadd608 33bdadd608")//PlayerID x 3 - no need to change.
			.writeString(ogetter.get("Name"))
			.writeHex("7f14")
			.writeVInt(ogetter.get("Trophies"))
			.writeHex("000000000000000021000000000008")

			.writeVInt(2)//Total Resources

			.writeSCID(5,1)//Gold SCID
			.writeVInt(ogetter.get("Gold"))

			.writeSCID(5,30)//Game Mode SCID
			.writeVInt(72000006)//Game Mode

			.writeHex(`00
				03
				3c0706
				3c0806
				3c0906
				00`) //Acheivements
			.writeHex(`02
				050b21
				050808

				04
				1a0001
				1a0101
				1a0d01
				1a0301

				0000`)

			.writeVInt(ogetter.get("Gems"))//Gems
			.writeVInt(ogetter.get("Gems"))//Gems

			.writeVInt(ogetter.get("XP"))
			.writeVInt(ogetter.get("Level"))
			
			.writeByte(0)

			.writeByte(9)//9=nameset+clan
			.writeHex("288fef69")//Clan ID

			.writeString(ogetter.get("Clan").Name)

			.writeHex("a802031900000c0d7e")

			.writeByte(ogetter.get("TrainingProgress"))

			.writeHex("0000000100b1e2f6f60787b6c2960bb8c1fe01");

	return o;

}

module.exports.OHD = (in_packet, opts = {}) => {
	var o = OwnHomeData(opts);
	return o;
}
