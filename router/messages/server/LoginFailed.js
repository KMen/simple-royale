function LoginFailed(opts) {
	var o = new librw.Writer();
	var ogetter = new optionGetter(opts, {
		ReasonCode: 10,
		fingerprint: "",
		redir: "",
		contentURL: "",
		updateURL: "",
		reason: "",
		secondsUntilMaintenanceEnds: settings.secondsUntilMaintenanceEnds
	});

	o.setID(20103)
		.writeRrsint(ogetter.get("ReasonCode"))
		.writeString(ogetter.get("fingerprint"))
		.writeString(ogetter.get("redir"))
		.writeString(ogetter.get("contentURL"))
		.writeString(ogetter.get("updateURL"))
		.writeString(ogetter.get("reason"))
		.writeRrsint(ogetter.get("secondsUntilMaintenanceEnds"))
		.writeBool(false)
		.writeString("");
	return o;
}

module.exports.maintenance = (in_packet) => {
	var o = LoginFailed({
		ReasonCode: 10
	});
	return o;
}

module.exports.banned = (in_packet) => {
	var o = LoginFailed({
		ReasonCode: 11,
		reason: "Please Clear your App Data."
	});

	return o;
}

module.exports.LoginFailed = LoginFailed;