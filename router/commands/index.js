var cmdenum = require("../../enums/cmds.json");

function get_handler (ID) {
	if (!cmdenum[ID]) {
		return {error: {message: "ID Unknown", code: 1}};
	}

	try {
		var handler = require("./" + cmdenum[ID].toString());
	} catch (e) {
		if (e.code == "MODULE_NOT_FOUND") {
			return {error: {message: "Command Not Handled", code: 2}};
		} else {
			return {error: {error: e, message: "Unknown Error", code: 3}};
		}
	}

	return {handle: handler.main, code: 0};
}

function handle (in_command) { //in_command.reader = librw.Reader, in_command.client = _clients[cKey], in_command.cKey = cKey, in_command.ID = ID;
	try {
		var handler = get_handler(in_command.ID);

		if (handler.error) {
			switch (handler.error.code) {
				case 1:
					console.error(`COMMAND ${in_command.ID.toString().red} is unknown.`);
					return;
				case 2: 
					console.error(`"${cmdenum[in_command.ID].red}" COMMAND (${in_command.ID.toString().red}) was not Handled!`);
					return;
				case 3:
					console.error(`An unknown error occurred while handling COMMAND ${in_command.ID.toString().red}`);
					throw handler.error.error;
					return;
				default:
					return;
			}
		} else {
			//everything should be in here
			console.log(`Handling COMMAND ${cmdenum[in_command.ID].green} (${in_command.ID.toString().green})...`)
			return handler.handle(in_command.reader, in_command);
		}



	} catch (err) {
		console.error(err.stack.red);
		_clients[in_command.cKey].connection.destroy();
		_clients[in_command.cKey] = {};
		delete _clients[in_command.cKey]
	}
};//MUST RETURN [{ID, Payload}] or [];

module.exports.handle = handle;
