var shortcards = require("../../../enums/shortcards");
var cardarray = require("../../../enums/cardarray");

function random (min, max) {
	var a = Math.floor(Math.random()*(max-min+1)+min);

	return a;
}

function main(){
	
	var pull = {
		"cards": [{name:"MegaKnight", count:1}],
		"gems":1,
		"gold":1
	};
/*
	var CCount = 3;

	for (var i = 0; i < CCount; i++) {
		pull.cards.push({
			name: cardarray[random(0,cardarray.length - 1)],
			count: random(1, 10)
		});
	}
*/
	pull.gems = random(1, 10000000);
	pull.gold = random(1, 10000000);

	console.log(JSON.stringify(pull));

	var o = new librw.Writer();
	 
	//o.writeHex("0100 03    39 00a1ddf517 0b    000000    3a 00a1ddf517 09    000000    38 00a1ddf517 a3    010000007f170202020a7f7f0000");
	

	o.writeByte(1)
		.writeBool(false) //isDraft
		.writeVInt(pull.cards.length);

	for (var index = 0; index < pull.cards.length; index++) {
		
		var card = pull.cards[index];

		o.writeVInt(shortcards[card.name][0])
			.writeByte(0)
			.writeVInt(25081697)
			.writeVInt(card.count)
			.writeByte(0)
			.writeByte(0)
			.writeByte(0); //Card Order
	};

	o.writeByte(127)
		.writeVInt(0)
		.writeVInt(pull.gems)
		.writeHex("02020a7f7f")
		.writeByte(0)
		.writeByte(0);

		console.log(o.Buf.toString("hex"));



//////////////////////////////////////////////////

	return {
		"ID": 210, 
		"payload": o.Buf
	};	
}

/////////////////////////////////////////////////

module.exports = {
	"main": main
}
 // 2 Gems
 // 11 SkeletonBallon
 // 9 FlyingMachine
 // 99 MegaKnight